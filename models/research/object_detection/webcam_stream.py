# For more info: http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html
import cv2
import numpy as np
from socket import *
#from cStringIO import StringIO
import threading
import sys
import pickle
import struct

class ServerThreaded(threading.Thread):
    def __init__(self,name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        print("starting thread")
        send_data(self.name)
        print("stopping thread")
    
def send_data(thread):
    while not done:
        connection_socket, addr = server_socket.accept()
        request = connection_socket.recv(1024).decode()
        response = data
        connection_socket.sendall(struct.pack(">L", size) + data)
        connection_socket.close()

done = False

encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]

# Playing video from file:
# cap = cv2.VideoCapture('vtest.avi')
# Capturing video from webcam:
cap = cv2.VideoCapture("/dev/video2")
cap.set(3,640)
cap.set(4,320)

server_port = 8080
server_socket = socket(AF_INET, SOCK_STREAM)

server_socket.bind(('',server_port))
server_socket.listen(1)

currentFrame = 0
data = ""
server = ServerThreaded("Server")
server.start()
while(True):
    try:
        # Capture frame-by-frame
        ret, frame = cap.read()
        result, frame = cv2.imencode('.jpg', frame, encode_param)

        data = pickle.dumps(frame,0)
        size = len(data)
        # To stop duplicate images
        currentFrame += 1

    except KeyboardInterrupt:
        done = True
        break

# When everything done, release the capture
cap.release()
