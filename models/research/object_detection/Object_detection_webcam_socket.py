import numpy as np
import cv2
import os
import sys

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")

import tensorflow as tf
from utils import label_map_util
from utils import visualization_utils as vis_util
import threading
import time
from socket import *
import pickle
import struct

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'inference_graph'

CWD_PATH = os.getcwd()

# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join(CWD_PATH,'training', 'labelmap.pbtxt')

NUM_CLASSES = 9
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480

server_name = "127.0.0.1"
server_port = 8080

class OutputFrame:
    def __init__(self):
        self.frame = np.zeros((IMAGE_HEIGHT,IMAGE_WIDTH,3))
        self.boxes = ()

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

class ClientThread(threading.Thread):
   def __init__(self, name):
      threading.Thread.__init__(self)
      self.name = name
   def run(self):
      print("Starting " + self.name)
      get_frame(self.name)
      print("Exiting " + self.name)

def get_frame(threadName):
    while not done:
    # setting client socket
        client_socket = socket(AF_INET, SOCK_STREAM)
        client_socket.connect((server_name, server_port))

        request = "dummy"
        client_socket.send(request.encode())
        payload_size = struct.calcsize(">L")
        data = b""
        
        while(len(data)<payload_size):
            response = client_socket.recv(4096)
            data += response

        packed_msg_size = data[:payload_size]
        data = data[payload_size:]
        msg_size = struct.unpack(">L", packed_msg_size)[0]

        while len(data) < msg_size:
            data += client_socket.recv(4096)

        frame_data = data[:msg_size]
        data = data[msg_size:]

        frame = pickle.loads(frame_data, fix_imports=True, encoding="bytes")
        frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)

        # tutup client socket
        client_socket.close()

        output_frame.frame = frame

class PredictorThread(threading.Thread):
   def __init__(self, name):
      threading.Thread.__init__(self)
      self.name = name
   def run(self):
      print("Starting " + self.name)
      predict(self.name)
      print("Exiting " + self.name)

def predict(threadName):
    while not done:
        image_np = output_frame.frame
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = detection_graph.get_tensor_by_name('detection_scores:0')
        classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')
        # Actual detection.
        output_frame.boxes = sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})

        global counter
        global distance_counter
        global warning_counter

        if(counter==5):
            for i, b in enumerate(output_frame.boxes[0][0]):
                if(output_frame.boxes[2][0][i] == 8 or output_frame.boxes[2][0][i] == 9):
                    if(output_frame.boxes[1][0][i]>0.85):
                        mid_x = (output_frame.boxes[0][0][i][3] + output_frame.boxes[0][0][i][1]) / 2
                        mid_y = (output_frame.boxes[0][0][i][2] + output_frame.boxes[0][0][i][0]) / 2
                        apx_distance = (1 - (output_frame.boxes[0][0][i][3] - output_frame.boxes[0][0][i][1]))
                        for i in range(20):
                            distance_counter += 1
                            distance_list.append((apx_distance,(mid_x,mid_y)))
                        if(apx_distance <=0.65):
                            if(apx_distance > 0.2 and apx_distance < 0.7):
                                for i in range(20):
                                    warning_counter += 1
                                    warning_list.append(("WARNING",(mid_x,mid_y)))
        else:
            counter += 1


if __name__ == '__main__':
    done = False
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    sess = tf.Session(graph=detection_graph)
    output_frame = OutputFrame()

    np_arr = np.random.rand(480,640,3)

    client_thread = ClientThread("Client Thread")
    predictor_thread = PredictorThread("Predictor Thread")
    client_thread.start()
    time.sleep(1)
    predictor_thread.start()

    counter = 0
    distance_counter = 0
    distance_list = []
    warning_counter = 0
    warning_list = []

    while True:
        if output_frame.boxes == ():
            to_show = output_frame.frame
        else:
            to_show = output_frame.frame
            vis_util.visualize_boxes_and_labels_on_image_array(
              to_show,
              np.squeeze(output_frame.boxes[0]),
              np.squeeze(output_frame.boxes[2]).astype(np.int32),
              np.squeeze(output_frame.boxes[1]),
              category_index,
              use_normalized_coordinates=True,
              line_thickness=8,
              min_score_thresh=0.85)

        if(distance_counter > 0):
            distance = distance_list.pop(0)
            distance_counter -= 1
            cv2.putText(to_show, '{}'.format(round(distance[0],2)), (int(distance[1][0]*640), int(distance[1][1]*480)), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,0,255), 2)

        if(warning_counter > 0):
            warning = warning_list.pop(0)
            warning_counter -= 1
            cv2.putText(to_show, '{}'.format(warning[0]), (int(warning[1][0]*640), int(warning[1][1]*480)-30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0,0,255), 2)

        cv2.imshow('frame', to_show)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            done = True
            break

    cv2.destroyAllWindows()
